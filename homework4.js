//detonatorTimer(3);
// 3
// 2
// 1
// BOOM!

function detonatorTimer(delay) {
    let i = delay;

    let interval = setInterval(() => {
        if(i === 0) {
            console.log('BOOM!');
            clearInterval(interval);
            return;
        }

        console.log(i--);

    }, 1000)
}

// Task 2

//detonatorTimer(3);
// 3
// 2
// 1
// BOOM!

function detonatorTimer(delay) {
	let i = delay;

    setTimeout(() => {

        if(i === 0) {
            console.log('BOOM!');
            return;
        }

        console.log(i);

        detonatorTimer(i - 1);

    }, 1000)
}

// Task 3

let me = {
    name: 'Vlad',
    age: '29',
    work: 'musician',
    instruments: ['guitar', 'drums', 'tuba'],
    genres: ['rock', 'surf', 'funk'],
    festivales: [
            {
                title: 'Woodstok',
                year: '1999'
            },
            {
                title: 'Sziget',
                year: '2004'
            },
            {
                title: 'Glastonbury',
                year: '2007'
            }
        ],
    introduce: function(){
        console.log(`My name is ${this.name ?? 'Somebody'}, i am ${this.age ?? '29 or not'}, i working ${this.work ?? 'or not'}`)
    },
    instrumentsSkills: function(){
        console.log(`I can play by ${this.instruments?.join(', ') ?? 'some music instruments'} in many genres: ${this?.genres.join(', ') ?? 'rock or something else'}`)
    },
    festivalStory: function() {
        console.log(`I was plaing with my music bands on some festivales: ${
            this.festivales?.map((festival) => {
                return `${festival.title} in ${festival.year}`
            }).join(', ')
            ?? 'but now i dont remember it'
        }`)
    }
}
// me.introduce();
// me.instrumentsSkills();
// me.festivalStory();

// Task 4

function introduce(){
    console.log(`My name is ${this.name ?? 'Somebody'}, i am ${this.age ?? '29 or not'}, i working ${this.work ?? 'or not'}`)
}

function instrumentsSkills(){
    console.log(`I can play by ${this.instruments?.join(', ') ?? 'some music instruments'} in many genres: ${this?.genres.join(', ') ?? 'rock or something else'}`)
}

function festivalStory() {
    console.log(`I was plaing with my music bands on some festivales: ${
        this.festivales?.map((festival) => {
            return `${festival.title} in ${festival.year}`
        }).join(', ')
        ?? 'but now i dont remember it'
    }`)
}

let myInfo = {
    name: 'Vlad',
    age: '29',
    work: 'musician',
    instruments: ['guitar', 'drums', 'tuba'],
    genres: ['rock', 'surf', 'funk'],
    festivales: [
            {
                title: 'Woodstok',
                year: '1999'
            },
            {
                title: 'Sziget',
                year: '2004'
            },
            {
                title: 'Glastonbury',
                year: '2007'
            }
        ]
}

let securedSelfIntroduce = () => introduce.call(myInfo)// якийсь код
let securedInstrumentsSkills = () => instrumentsSkills.call(myInfo)// якийсь код
let securedSelfFestivalStory = () => festivalStory.call(myInfo)// якийсь код

// setTimeout(securedSelfIntroduce, 1000); // виведе коректний результат
// setTimeout(securedInstrumentsSkills, 2000); // виведе коректний результат
// setTimeout(securedSelfFestivalStory, 3000); // виведе коректний результат


// Task 5

function someFunction(a, b){
    console.log(a + b)
} // тут напишіть довільну функцію яка робить якусь роботу зі своїми аргументами та виводить результат в консоль

function slower(func, seconds) {
    // тут ваш код для декоратора
    return function(...args){
        console.log("Chill out, you will get you result in 5 seconds")
        setTimeout(() => func(...args), seconds * 1000)
    }
}

let slowedSomeFunction = slower(someFunction, 5); // обгортаєте свою довільну функцію 'someFunction' в декоратор і задає значення вповільнення

slowedSomeFunction(2, 3) // викликаєте декоратор

// виведе в консоль "Chill out, you will get you result in 5 seconds
//...через 5 секунд виведе результат роботи 'someFunction'