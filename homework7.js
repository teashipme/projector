class Music {

    #lets;

    constructor(name, genre){
        this.#lets = 'lets';
        this.name = name;
        this.genre = genre;
        this.phrase = `This is ${this.name}`;
    }

    play(){
        console.log(`${this.phrase}, ${this.#lets} play ${this.genre}`)
    }

    download(){
        console.log(`${this.phrase}, ${this.#lets} download their songs`)
    }

    hate(){
        console.log(`${this.phrase}, i hate ${this.genre} music`)
    }
}

class Genre extends Music {
    #counter;

    constructor(name, genre, instrument){
        super(name, genre);
        this.instrument = instrument;
        this.#counter = 0;
    }

    rock(){
        this.#counter += 1;

        if(this.#counter > 3) {
            console.log('Im tired');
        } else {
            console.log(`${this.phrase}, lets rock! Play solo on ${this.instrument}`);
        }
    }

    funk(){
        console.log(`${this.phrase}, lets dance!`);
    }

    indie(){
        console.log(`${this.phrase}, lets singing together!`);
    }
}

class Band extends Genre{
    constructor(name, genre, instrument, howMuch){
        super(name, genre, instrument, howMuch);
        this.name = name;
        this.howMuch = howMuch;
    }

    story(){
        console.log(`This is ${this.name}, and they play in ${this.genre} genre`);
    }

    isItBand(){
        if(this.howMuch < 4){
            console.log(`This is ${this.name}, and its band`);
        } else {
            console.log(`This is ${this.name}, and its orcentra`);
        }
    }
}

class Instrument extends Band {
    constructor(name, genre, instrument, howManyStrings){
        this.name = name;
        this.genre = genre;
        this.instrument = instrument;
        this.howManyStrings = howManyStrings;
    }

    guitar() {
        if(this.instrument === 'guitar') {
            if(this.howManyStrings > 4) {
                console.log(`This is ${this.instrument}`);
            } else {
                console.log(`This is buss`);
            }
        }
    }
}

let a = new Band('Queen', 'rock', 'guitar')

a.rock()
a.rock()
a.rock()
a.rock()
