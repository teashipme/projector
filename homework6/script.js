"use strict"

const input = document.querySelector('.add-input');
const add = document.querySelector('.add');
const list = document.querySelector('.list');
const search = document.querySelector('.search');
const clean = document.querySelector('.clean');
let counter = 0;
let toDoItemsStorageObj = {};

if(localStorage.getItem('toDoItems') !== null) {
    let obj = JSON.parse(localStorage.getItem('toDoItems'));

    Object.keys(obj).forEach(key => {
        toDoItemsStorageObj[key] = obj[key];
       
        createElement(obj[key], key)
    });
} else {
    toDoItemsStorageObj = {};
}

add.addEventListener('click', () => {
    if(input.value == '') {
        return;
    }

    if(localStorage.getItem('toDoItems') !== null) {
        
        let arr = Object.keys(JSON.parse(localStorage.getItem('toDoItems')));
        let num = 0;

       arr.forEach(item => {
        if(item > num) {
            num = item
        }
       })
       counter = Number(num) + 1;
    } else {
        counter++
    }
    
    createElement(input.value, counter);
    localeStorageHandler(input.value, counter);
    input.value = '';
});

function createElement(value, index){

    const item = document.createElement('li');
    item.innerHTML = `
        <span>${value}</span>
       
            <button class="remove">x</button>
            <button class="edit">&hellip;</button>
            <input type="text" class="edit-input">
            <button class="edit-btn">e</button>
        
    `;
    item.setAttribute('id', index)
    list.appendChild(item);
}


list.addEventListener('click', (e) => {
    if(e.target.classList.contains('remove')){
        removeHandler(e);
    }
    if(e.target.classList.contains('edit')){
        editHandler(e)
    }
});

function localeStorageHandler(value, ind){
    if(localStorage.getItem('toDoItems') !== null) {
        toDoItemsStorageObj = JSON.parse(localStorage.getItem('toDoItems'));
    }
    toDoItemsStorageObj[ind] = value;
    localStorage.setItem('toDoItems', JSON.stringify(toDoItemsStorageObj))
}

function removeHandler(e){
    let elementId = e.target.parentElement.getAttribute('id');
    
    delete toDoItemsStorageObj[elementId];
    localStorage.setItem('toDoItems', JSON.stringify(toDoItemsStorageObj));

    e.target.parentElement.remove();
}

function editHandler(e){
    let text = e.target.parentElement.querySelector('span').innerHTML;
    let id = e.target.parentElement.getAttribute('id')
    let editInput = e.target.parentElement.querySelector('.edit-input');
    let editBtn = e.target.parentElement.querySelector('.edit-btn');

    editInput.value = text;
    editInput.classList.add('active');
    editBtn.classList.add('active');
    editBtn.addEventListener('click', () => {
        e.target.parentElement.querySelector('span').innerHTML = editInput.value
        editInput.classList.remove('active');
        editBtn.classList.remove('active');

        if(localStorage.getItem('toDoItems') !== null) {
            toDoItemsStorageObj[id] = editInput.value;

            localStorage.setItem('toDoItems', JSON.stringify(toDoItemsStorageObj));
        }
    })
}

search.addEventListener('keyup', () => {
    
    let list = document.querySelectorAll('ul li');
    
    list.forEach(item => {
        item.classList.remove('find')
        if(item.querySelector('span').innerHTML == search.value) {
            item.classList.add('find')
        }
    })
})

clean.addEventListener('click', () => {
    input.value = '';
    toDoItemsStorageObj = {};
    list.innerHTML = '';
    localStorage.clear();
});