"use strict";

// Github
// UI

const GITHUB_API = "https://api.github.com";

const searchInput = document.querySelector(".searchUser");

class Github {
  constructor() {
    this.clientId = "4a4c89dbbeb08a06ac62";
    this.clientSecret = "d423176ee0534ef9599747ff38b8d3ec121ffd24";
  }
  
  async getUser(userName) {
    
    const response = await fetch(
      `${GITHUB_API}/users/${userName}?client_id=${this.clientId}&client_secret=${this.clientSecret}`
    );
    const user = await response.json();

    return user;
  }

  async getRepos(userName) {
    
    const response = await fetch(
      `${GITHUB_API}/users/${userName}/repos?client_id=${this.clientId}&client_secret=${this.clientSecret}`
    );
    const repos = await response.json();

    return repos;
  }
}



class UI {
  constructor() {
    this.profile = document.querySelector(".profile");
    this.repos = document.querySelector(".repos");
  }

  showProfile(user) {
    this.profile.innerHTML = `
    <div class="card card-body mb-3">
    <div class="row">
      <div class="col-md-3">
        <img class="img-fluid mb-2" src="${user.avatar_url}">
        <a href="${user.html_url}" target="_blank" class="btn btn-primary btn-block mb-4">View Profile</a>
      </div>
      <div class="col-md-9">
        <span class="badge badge-primary">Public Repos: ${user.public_repos}</span>
        <span class="badge badge-secondary">Public Gists: ${user.public_gists}</span>
        <span class="badge badge-success">Followers: ${user.followers}</span>
        <span class="badge badge-info">Following: ${user.following}</span>
        <br><br>
        <ul class="list-group">
          <li class="list-group-item">Company: ${user.company}</li>
          <li class="list-group-item">Website/Blog: ${user.blog}</li>
          <li class="list-group-item">Location: ${user.location}</li>
          <li class="list-group-item">Member Since: ${user.created_at}</li>
        </ul>
      </div>
    </div>
  </div>
  <h3 class="page-heading mb-3">Latest Repos</h3>
    `;
  }

  showRepos(user){
    this.repos.innerHTML = '';

    for (let i = 0; i < 5; i++) {
        if (user[i] && user[i].name) {
            this.repos.innerHTML += `<div class="repos">${user[i].name}</div>`;
        }
    }
  }

  clearProfile() {
    this.profile.innerHTML = "";
  }

  clearReposProfile() {
    this.repos.innerHTML = "";
  }

  showAlert(message, className) {
    this.clearAlert();
    const div = document.createElement("div");

    div.className = className;
    div.innerHTML = message;

    const search = document.querySelector(".search");

    search.before(div);

    setTimeout(() => {
      this.clearAlert();
    }, 3000);
  }

  clearAlert() {
    const alert = document.querySelector(".alert");
    if (alert) {
      alert.remove();
    }
  }
}

const github = new Github();
const ui = new UI();


function debounce(func, timeout = 300){
    let timer;
    return (...args) => {
      clearTimeout(timer);
      timer = setTimeout(() => { func.apply(this, args); }, timeout);
    };
  }
  async function saveInput(e){
    const inputValue = e.target.value;

    if (inputValue !== "") {
        const userData = await github.getUser(inputValue);
        const userReposData = await github.getRepos(inputValue);

        if (userData.message === "Not Found") {
            ui.clearProfile();
            ui.clearReposProfile();
            
            return ui.showAlert(userData.message, "alert alert-danger");
        }

        ui.showRepos(userReposData)
        ui.showProfile(userData);
        return;
    }

    ui.clearProfile();
    ui.clearReposProfile();
  }
  
const processChange = debounce((event) => saveInput(event));

searchInput.addEventListener("input", processChange);