"use strict"

// Task 1

const userNames = ["Петрик Ольга Іванівна", "Гнатюк Петро Антонович", "Рудко Андрій Опанасович"];
let initials;

initials = userNames.map(function(item){

    let words = item.split(' ');

    let initialsArray = words.map(function(word){
        return word[0] + '.';
    })

    return initialsArray.join('');
});

initials.sort();

console.log(initials); // [ "Г.П.А.", "П.О.І.", "Р.А.О."]



// Task 2

const userNames2 = ['Петро', 'Емма', 'Юстин', 'Ілля', 'Марта', 'Яна', 'Василь', 'Антон', 'Олена'];
let filteredNames;
let filteredNames2;

const vowels = ["а", "о", "у", "е", "і", "и", "є", "ї", "я", "ю",];

// variant 1
filteredNames = userNames2.filter(function(item){
    for(let i=0;i<vowels.length;i++){
        if(item[0].toLowerCase() == vowels[i]){
            return item;
        }
    }
    
});

console.log(filteredNames); // ['Емма', 'Юстин', 'Ілля', 'Яна', 'Антон', 'Олена']


//variant2

filteredNames2 = userNames2.filter(function(item){
    return item[0].match(/[аоуеіиїяює]/gi);
})

console.log(filteredNames2) // ['Емма', 'Юстин', 'Ілля', 'Яна', 'Антон', 'Олена']



// Task 3

const currentMaxValue = 4589;
let reverseMaxValue;

let currentMaxValueArr = currentMaxValue.toString().split('').reverse();
reverseMaxValue = Number(currentMaxValueArr.join(''));

console.log(reverseMaxValue); // 9854
console.log(typeof reverseMaxValue); // 'number'




// Task 4

const resultsArray = [1, 2, [3, [4]]];
let productOfArray;

// variant 1

productOfArray = resultsArray.flat(Infinity).reduce(function(a, b){
    return a * b;
});

console.log(productOfArray); // 24