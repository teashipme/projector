export function holidays(){
    const form = document.querySelector('.tab2 form');
    const countries = document.querySelector('#countries');
    const table = document.querySelector('.tab2 table');

    fetch('https://calendarific.com/api/v2/countries?api_key=otGhYViitqqTvVk87GGrZpghlDHf69sx')
    .then((response) => response.json())
    .then((data) => {
        data.response.countries.forEach(element => {
            const option = document.createElement('option');
            option.innerHTML = element.country_name;
            option.value = element['iso-3166']
            countries.appendChild(option)
        });
    })
    .catch(() => addError());

    const yearSelect = document.querySelector('#date');

    for(let i = 2001; i < 2050; i++){
        const option = document.createElement('option');
        option.value = i;
        option.innerHTML = i;
        if(i === Number(new Date().getFullYear())){
            option.setAttribute('selected', 'selected')
        }
        yearSelect.appendChild(option)
    }

    function addToTable(data){
        document.querySelectorAll('.tab2 .localstorage').forEach(item => item.remove())

        data.forEach(element => {
            const newDate = new Date(element.date.iso)
            table.innerHTML += `
                <tr class="localstorage">
                    <td>${newDate.getDate()} ${newDate.toLocaleString('default', { month: 'short' })} ${newDate.getFullYear()}</dt>
                    <td>${element.name}</dt>
                </tr>
            `
        });
    }

    let arr = null;

    function getHolidays() {
        fetch(`https://calendarific.com/api/v2/holidays?api_key=otGhYViitqqTvVk87GGrZpghlDHf69sx&country=${countries.value}&year=${yearSelect.value}`)
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            arr = data.response.holidays;
            addToTable(arr);
            sortBtn.classList.add('active')
        })
        .catch(() => addError())
    }

    function addError(){
        form.innerHTML += `<div class="error">Ooooops, error ;(</div>`;
    }

    const sortBtn = document.querySelector('.sort');

    sortBtn.addEventListener('click', ()=>{
        sortBtn.classList.toggle('sorted');
        arr.sort(function () {
            return -1;
        })
        
        addToTable(arr);
    });

    countries.addEventListener('change', () => {
        yearSelect.removeAttribute('disabled');
        getHolidays();
    });

    yearSelect.addEventListener('change', () => {
        getHolidays();
    });
}