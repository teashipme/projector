export function calendar(){
    const date1 = document.querySelector('#date1');
    const date2 = document.querySelector('#date2');
    const form = document.querySelector('form');
    const btn = document.querySelectorAll('.btn');
    const lastResultElement = document.querySelector('.last-result');
    let dateDistanceMiliseconds = null;
    let result = null;

    if(JSON.parse(localStorage.getItem('result')) !== null) {
        lastResultHandler(JSON.parse(localStorage.getItem('result')));
    }
    
    date1.addEventListener('change', () => {
        if(date1.value === '') {
            return date2.setAttribute('disabled', 'disabled');
        }
    
        if(new Date(date1.value) > new Date(date2.value)) {
            return date2.value = '';
        }
    
        date2.removeAttribute('disabled');
        date2.setAttribute('min', date1.value);
    
        dateDistance();
        removeBtnActive();
    });
    
    date2.addEventListener('change', ()=> {
        dateDistance();
        addDays.value = 0;
        removeBtnActive();
    });
    
    function removeBtnActive(){
        btn.forEach(item => item.classList.remove('active'));
    }
    
    function addBtnActive(btn){
        btn.parentElement.querySelectorAll('.btn').forEach(item => item.classList.remove('active'))
        btn.classList.add('active');
    }
    
    function dateDistance(){
        dateDistanceMiliseconds = new Date(date2.value) - new Date(date1.value);
        result = Math.floor(dateDistanceMiliseconds / (1000 * 60 * 60 * 24));
    }
    
    function addDays(val){
        const day = 60 * 60 * 24 * 1000;
        const week = new Date(new Date(date1.value).getTime() + day * 7);
        const month = new Date(new Date(date1.value).getTime() + day * 30);
        
        switch (val) {
            case '7':
                date2.value = week.toISOString().split('T')[0];
                dateDistance();
                break;
            case '30':
                date2.value = month.toISOString().split('T')[0];
                dateDistance();
                break;
        }
    }
    
    const countWeekDaysBetween = (startDate, endDate) => 
      Array
        .from({ length: (endDate - startDate) / (1000 * 3600 * 24) })
        .reduce(count => {
          if (startDate.getDay() % 6 !== 0) count++;
          startDate = new Date(startDate.setDate(startDate.getDate() + 1));
          return count;
        }, 0);
    
    const countWeekendDaysBetween = (startDate, endDate) => 
      Array
        .from({ length: (endDate - startDate) / (1000 * 3600 * 24) })
        .reduce(count => {
          if (startDate.getDay() % 6 == 0) count++;
          startDate = new Date(startDate.setDate(startDate.getDate() + 1));
          return count;
        }, 0);
    
    
    form.addEventListener('click', (e)=> {
        addBtnActive(e.target);
    
        switch(e.target.value){
            case '7':
                addDays('7');
                break;
            case '30':
                addDays('30');
                break;
            case 'all':
                result = Math.floor(dateDistanceMiliseconds / (1000 * 60 * 60 * 24));
                break;
            case 'weekdays':
                result = countWeekDaysBetween(new Date(date1.value), new Date(date2.value));
                break;
            case 'weekend':
                result = countWeekendDaysBetween(new Date(date1.value), new Date(date2.value));
                break;
            case 'days':
                result = Math.floor(dateDistanceMiliseconds / (1000 * 60 * 60 * 24));
                break;
            case 'hours':
                result = Math.floor(dateDistanceMiliseconds / (1000 * 60 * 60));
                break;
            case 'minutes':
                result = Math.floor(dateDistanceMiliseconds / (1000 * 60));
                break;
            case 'seconds':
                result = Math.floor(dateDistanceMiliseconds / 1000);
                break;
        }
    })
    let arr = [];
    function setLocalStorage(){
        if(JSON.parse(localStorage.getItem('result')) !== null) {
            arr = JSON.parse(localStorage.getItem('result'))
        }
        let obj = {
            date1: date1.value,
            date2: date2.value,
            result
        }
        
        arr.push(obj)
        
        localStorage.setItem('result', JSON.stringify(arr))
    }

    function lastResultHandler(data){
        document.querySelectorAll('.tab1 .localstorage').forEach(item => item.remove())

        data.slice(-10).forEach(item => {
            lastResultElement.innerHTML += `
                <tr class="localstorage">
                    <td>${item.date1}</td>
                    <td>${item.date2}</td>
                    <td>${item.result}</td>
                </tr>
            `
        })
    }
    
    form.addEventListener('submit', (e) => {
        e.preventDefault();
    
        const resultElement = document.querySelector('.result');
        resultElement.innerHTML = `Result: ${result}`;
    
        setLocalStorage();

        lastResultHandler(JSON.parse(localStorage.getItem('result')));
        
    })
}