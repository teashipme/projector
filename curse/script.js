import { calendar } from "./calendar.js";
import { holidays } from "./holidays.js";

const tabSwitch = document.querySelector('.tab');
const tab1 = document.querySelector('.tab1');
const tab2 = document.querySelector('.tab2');

calendar();

tabSwitch.addEventListener('click', (e)=> {
    if(e.target.classList.contains('active')){
        return
    }
    
    tabSwitch.querySelectorAll('div').forEach(item => item.classList.remove('active'))
    e.target.classList.add('active');

    tab2.classList.toggle('active');
    tab1.classList.toggle('active');
    holidays();
})