console.log('start');

const promise1 = new Promise((resolve, reject) => {
console.log(1)
resolve(2)
})

promise1.then(res => {
console.log(res)
})

console.log('end');

// сочатку виконується console.log('start')
// далі створюється const promise1 і Promise в цей момент в статусі pending
// далі promise1 починає виконувати асинхронно команди:
// console.log(1)
// далі оскільки код асинхронний, то виконається console.log('end')
// і оскільки не має помилок resolve передасть аргумент 2 в console.log(res)



Promise.resolve(1)
		.then((x) => x + 1) // до аргументу додається 1
		.then((x) => { throw new Error('My Error') }) //виводимо помилку
		.catch(() => 1) // зловивши помилку повертаємо 1
		.then((x) => x + 1) // до агрументу додаємо 1
		.then((x) => console.log(x)) // виводимо результат додавання аргументу + 1
		.catch(console.error) // якщо щось з вищевказанного втратимо - отримаємо помилку



const promise = new Promise(res => res(2));
	promise.then(v => {
	        console.log(v); // виводимо агрумент резолвду 2
	        return v * 2; // передаємо далі результат множення
	    })
	    .then(v => {
	        console.log(v); // виводимо результат попереднього then 4
	        return v * 2; // передаємо далі результат множення
	    })
	    .finally(v => {
	        console.log(v); // finally не отримує агрументів, отже не знає що в нього передають, тому для console.log(v) v - нічого не значить і має значення undefined
	        return v * 2; // це множення далі не передається
	    })
	    .then(v => {
	        console.log(v); // виводить результат попереднього множення в then 8
	    });