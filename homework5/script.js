// homework 5, Task 2

const turnOnData = {
    btnText: 'Turn On',
    dateText: 'Last turn off: ',
    status: true
}

const turnOffData = {
    btnText: 'Turn Off',
    dateText: 'Last turn on: ',
    status: false
}

const containerElement = document.querySelector('#container');
const btnElement = document.querySelector('#btn');
const dateElement = document.querySelector('#date');

if(localStorage.getItem('turn-status') !== null) {
    containerElement.classList.add(JSON.parse(localStorage.getItem('turn-status')).background ? 'off' : 'on');
    btnElement.innerHTML = JSON.parse(localStorage.getItem('turn-status')).btnText;
    dateElement.innerHTML = JSON.parse(localStorage.getItem('turn-status')).date;
} else {
    containerElement.classList.remove('off');
    btnElement.innerHTML = turnOffData.btnText;
}

function getDate(){

    const date = new Date();

    function smallerThanTen(num) {
        return num < 10 ? '0' + num : num;
    }

    return `
            ${smallerThanTen(date.getDay())}-${smallerThanTen(date.getMonth())}-${date.getFullYear()} 
            ${smallerThanTen(date.getHours())}:${smallerThanTen(date.getMinutes())}:${smallerThanTen(date.getSeconds())}
            `;
}

function turnHandler(data){
    btnElement.innerHTML = data.btnText;
    dateElement.innerHTML = data.dateText + getDate();

    const turnStaus = {
        btnText: data.btnText,
        date: data.dateText + getDate(),
        background: data.status
    }

    localStorage.setItem('turn-status', JSON.stringify(turnStaus));
}

btnElement.addEventListener('click', () => {
    containerElement.classList.toggle('off');

    if(containerElement.classList.contains('off')) {
        turnHandler(turnOnData);
        return
    }

    turnHandler(turnOffData);
})
