// Task 1

function durationBetweenDates(startDate = '01 Jul 1993', endDate = '03 Aug 1995', integer = 'days'){
    
    startDate = Date.parse(startDate);
    endDate = Date.parse(endDate);
    result = endDate - startDate;

    switch(integer){
        case 'seconds':
            result /= 1000;
            break;
        case 'minutes':
            result /= (1000 * 60);
            break;
        case 'hours':
            result /= (1000 * 60 * 60);
            break;
        case 'days':
            result /= (1000 * 60 * 60 * 24);
            break;
        default:
            result /= (1000 * 60 * 60 * 24);
            break;
    }

    result == 1 ? integer = integer.slice(0, -1) : integer;

    console.log(`${result} ${integer}`)
}

durationBetweenDates('02 Aug 1985', '03 Aug 1985', 'seconds');
durationBetweenDates('02 Aug 1985', '03 Aug 1985', 'minutes');
durationBetweenDates('02 Aug 1985', '03 Aug 1985', 'hours');
durationBetweenDates('02 Aug 1985', '03 Aug 1985', 'days');
durationBetweenDates();



// Task 2 

const priceData = {
    Apples: '23.4',
    BANANAS: '48',
    oRAngGEs: '48.7584',
    };
    
    function optimizer(data) {
        let newObject = {};

        for (let key in data) {

            const newKey = String(key).toLowerCase();
            const value = Number(data[key]).toFixed(2);

            newObject[newKey] = String(value)
        }

        return newObject;
    };
    
    let updatedPriceData = optimizer(priceData);
    
    console.log(updatedPriceData) // {apples: '23.40', bananas: '48.00', oranges: '48.76'}



// Task 3

function recursiveOddSumTo(number) {

    if(number == 1) {
        return number
    }

    if(number % 2 !== 0) {
        return number + recursiveOddSumTo(number - 1)
    }

    return recursiveOddSumTo(number - 1)
}

console.log(recursiveOddSumTo(1)) // 1
console.log(recursiveOddSumTo(10)) // 25




// Task 4

function iterativeOddSumTo(number) {

    let iterator = 0;

    for(let i=0; i < number + 1; i++){

        if(i % 2 !== 0) {
            iterator += i;
        }
    }

    return iterator;
};
    
console.log(iterativeOddSumTo(1)) // 1
console.log(iterativeOddSumTo(10)) // 25